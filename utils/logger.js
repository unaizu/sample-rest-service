const Bunyan = require('bunyan');
// const { prototype } = require('bunyan');
const settings = require('../settings');

/*
// in case you don't want to use the extended class uncomment the below and comment the class
const Logger = Bunyan.createLogger({
    name: settings.app,
    level: settings.loggingLevel
    //streams: [{
    //    type: 'rotating-file',
    //    path: './logs/application.log',
    //    level: settings.loggingLevel,
    //    period: '12h',   // daily rotation
    //    count: 60        // how many copies days logged = period in hours * count / 24
    //}]
});
*/

class MyLogger extends Bunyan {
  constructor(options, _childOptions, _childSimple) {
    // Nothing special for child loggers.
    if (_childOptions) {
      // eslint-disable-next-line constructor-super
      return super(options, _childOptions, _childSimple);
    }

    // Default options.
    let classOptions = options;
    classOptions = {
      name: 'logger',
      serializers: Bunyan.stdSerializers,
      ...(classOptions || {}),
    };

    // Formatters.
    // TODO: needed?

    super(classOptions, _childOptions, _childSimple);
  }

  // eslint-disable-next-line class-methods-use-this
  getCaller() {
    const error = new Error().stack.split('\n')[3].trim().split('/');
    return `${error[error.length - 1].replace(')', '')}`;
  }

  debug(message) {
    let thisMessage = message;
    if (typeof thisMessage === 'object') {
      thisMessage = JSON.stringify(thisMessage);
    }
    super.debug(` 🟣 ${this.getCaller()} - ${thisMessage}`);
  }

  info(message) {
    let thisMessage = message;
    if (typeof thisMessage === 'object') {
      thisMessage = JSON.stringify(thisMessage);
    }
    super.info(` 🟢 ${this.getCaller()} - ${thisMessage}`);
  }

  warn(message) {
    let thisMessage = message;
    if (typeof thisMessage === 'object') {
      thisMessage = JSON.stringify(thisMessage);
    }
    super.warn(` 🟠 ${this.getCaller()} - ${thisMessage}`);
  }

  error(message) {
    let thisMessage = message;
    if (typeof thisMessage === 'object') {
      thisMessage = JSON.stringify(thisMessage);
    }
    super.error(` 🔴 ${this.getCaller()} - ${thisMessage}`);
  }

  trace(message) {
    let thisMessage = message;
    if (typeof thisMessage === 'object') {
      thisMessage = JSON.stringify(thisMessage);
    }
    super.trace(` 🔔 ${this.getCaller()} - ${thisMessage}`);
  }

  test(message) {
    let thisMessage = message;
    if (typeof thisMessage === 'object') {
      thisMessage = JSON.stringify(thisMessage);
    }
    super.trace(` 😜 😳 🦠 ${this.getCaller()} - ${thisMessage} 😬 😖`);
  }
}

const Logger = new MyLogger({
  name: settings.app,
  level: settings.loggingLevel,
});

Logger.info('Logger Starting...');

module.exports = Logger;
