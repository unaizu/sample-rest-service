'use strict';

const Employee = require('../models/employee');
const Logger = require('../utils/logger');

/*
 * This module is used in order to fetch an object whe the ID or TAG is passed
 */
const getData = (type, id, req) => {
  return new Promise((resolve, reject) => {
    // if we didn't get an id reject
    if (id === undefined) {
      Logger.error(`[getData] id not set`);
      reject({ message: 'invalid ID' });
    }

    try {
      // initialise data
      let data = undefined;
      if (type === 'employee') {
        data = Employee.getEmployeeWithId(id);
      }
      if (data) {
        // if we got data resolve it else reject it
        resolve(data);
      } else {
        reject({ message: 'No data returned' });
      }
    } catch (err) {
      Logger.error(`[getData] error: ${err}`);
      reject(err);
    }
  });
};

// module starts here
module.exports = (type) => {
  return (req, res, next, value) => {
    try {
      // we call the get data method and check if something is returned via Promise
      getData(type, value, req)
        .then(function (data) {
          // if promise is returned
          Logger.debug(`[findObject] got data for type: ${type} value: ${value} data: ${JSON.stringify(data)}`);
          // create the word plural
          const typePlural = `${type}s`;
          // if we got an array get the first index:
          if (typeof data === Array) {
            data = data[0];
          }
          // find the item and see if it matches the value we are searching for
          // if the object does not have an Id index we can use the following if statement
          // in order to change the search parameters
          let obj;
          // in this example we assume the stock object does not have an Id index
          // instead it has the index symbol so we use this to filter through
          if (type === 'stock') {
            obj = data[typePlural].find((t) => t.symbol === value);
          } else {
            obj = data[typePlural].find((t) => t.Id === value * 1);
          }

          if (obj) {
            // if an object matches the Id
            req[type] = obj; // store it on the request header with it's type as index
            next();
          } else {
            // else return 404 not found
            const status = 404;
            res.status(status).json({ status, error: `Invalid ${type} Id` });
            Logger.error(`[findObject] returned obj is invalid ${obj}`);
          }
        })
        .catch(function (err) {
          const status = err.status ? err.status : 400;
          res.status(status).json({ status, error: err.message });
          Logger.error(`[findObject] there was an error getting data ${err}`);
        });
    } catch (err) {
      const status = err.status ? err.status : 500;
      res.status(status).json({ status, error: err.message });
      Logger.error(`[findObject] there was an error getting data ${err}`);
    }
  };
};
