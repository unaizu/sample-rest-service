const Logger = require('../utils/logger');

describe('Testing Logger', () => {
  it('Test Debug Logging', () => {
    Logger.debug('Testing debug');
    expect(true).toEqual(true);
  });
  it('Test Info Logging', () => {
    Logger.info('Testing info');
    expect(true).toEqual(true);
  });
  it('Test Warn Logging', () => {
    Logger.warn('Testing warn');
    expect(true).toEqual(true);
  });
  it('Test Error Logging', () => {
    Logger.error('Testing error');
    expect(true).toEqual(true);
  });
  it('Test Trace Logging', () => {
    Logger.trace('Testing trace');
    expect(true).toEqual(true);
  });
  it('Test Test Logging', () => {
    Logger.test('Testing test');
    expect(true).toEqual(true);
  });
});
