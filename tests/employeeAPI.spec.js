const express = require('express');
const employeeRoutes = require('../routes/v1/employees');
const request = require('supertest');

const app = express(); // create an instance of an express app, a fake express app
app.use('/employees', employeeRoutes);

let firstEmployee;
describe('Testing Employee API Routes', () => {
  it('GET /employees - success', async () => {
    const { body } = await request(app).get('/employees');
    expect(body).toEqual({ status: 200, data: { employees: [{ Id: 1 }, { Id: 2 }] } });
    // store the first employee so we can use it on our next test
    firstEmployee = body.data.employees[0];
  });
  it('GET /employees/1 - success', async () => {
    const { body } = await request(app).get(`/employees/${firstEmployee.Id}`);
    expect(body.data.employees[0]).toEqual(firstEmployee);
  });
});
