require('dotenv').config({ silent: true });

module.exports = {
  port: process.env.PORT || 3005,
  env: process.env.NODE_ENV || 'development',
  app: process.env.APP_NAME || 'load-testing-backend',
  issuer: process.env.APP_ISSUER,
  subject: process.env.APP_SUBJECT,
  audience: process.env.APP_AUDIENCE,
  version: process.env.APP_VERSION,
  audit_logging: process.env.APP_AUDIT_LOGGING,

  loggingLevel: process.env.LOGGING_LEVEL || 'trace',

  jwt: {
    app_secret: process.env.APP_SECRET,
    private_key: process.env.APP_PRIVATE_KEY,
    public_key: process.env.APP_PUBLIC_KEY,
    expires_in: process.env.APP_EXPIRES_IN,
    sign_options: {
      issuer: process.env.APP_ISSUER,
      subject: process.env.APP_SUBJECT,
      audience: process.env.APP_AUDIENCE,
      expiresIn: process.env.APP_EXPIRES_IN,
      algorithm: 'RS256',
    },
  },

  alpha_vantage: {
    api_key: process.env.AV_API_KEY,
    host: process.env.AV_API_HOST,
    app: process.env.AV_API_APP_NAME,
  },

  twilio: {
    account_sid: process.env.TWILIO_ACC_SID,
    account_token: process.env.TWILIO_ACC_TOKEN,
  },

  cors: {
    whitelist: process.env.CORS_WHITELIST.split(','),
  },
};
