# Sample Test API Service - Changelog

## v1.0.1

Added findObject class
Added Husky, Lint and Prettier
Added tests

## v1.0.0

Initial Release
