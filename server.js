const app = require('express')();
// const swaggerJSDoc = require('swagger-jsdoc');
// const swaggerUi = require('swagger-ui-express');
const bodyParser = require('body-parser');

// load the settings
const http = require('http');
const settings = require('./settings');

// let fs = require('fs');
// let https = require('https');

const Logger = require('./utils/logger');

Logger.info('App Starting...');

// check if we've got a JWT token key
if (!settings.jwt.app_secret) {
  Logger.error('FATAL ERROR: JWT token key not found');
  process.exit(1);
}

// deal with CORS
app.use((req, res, next) => {
  // eslint-disable-next-line prefer-destructuring
  const whitelist = settings.cors.whitelist;

  // eslint-disable-next-line prefer-destructuring
  const origin = req.headers.origin;
  if (whitelist.indexOf(origin) > -1) {
    res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, X-Access-Token, Accept, Authorization, CurrentTime'
  );
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// load the routes, we will rely on index.js for the root of the tree and branch from there
const routes = require('./routes/v1');

app.all('*', async (req, res, next) => {
  // if this is a preflight request respond with ok
  if (req.method === 'OPTIONS') {
    res.status(200).json({ status: 200, data: 'Preflight request successful' });
    return;
  }

  // you could check headers here for custom items to improve security
  next();
});

// import any middlewares you want to use here
const middlewares = require('./middlewares');

app.use(middlewares.auditLogger);

// define global path
app.use('/api/v1/', routes);

Logger.info('App settings done');

/*
// define HTTPS server
var httpsServer = https.createServer(credentials, app);
httpsServer.listen(settings[settings.env].ssl.port, () => {
    Logger.info(`HTTPS Server Listening on port ${settings[settings.env].ssl.port}`);
});
*/

// define REST HTTP server
const httpServer = http.createServer(app);
httpServer.listen(settings.port, () => {
  Logger.info(`HTTP Server Listening on port ${settings.port}`);
});

module.exports = app;
