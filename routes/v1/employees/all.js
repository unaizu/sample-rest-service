const Employee = require('../../../models/employee');
const Logger = require('../../../utils/logger');

module.exports = async (req, res) => {
  try {
    // get all the employees
    await Employee.getAllEmployees(req, res).then((data) => {
      Logger.debug(`[REST employees all] about to return ${JSON.stringify(data)}`);
      res.status(200).json({ status: 200, data });
    });
  } catch (err) {
    const status = err.status ? err.status : 500;
    Logger.error(`[REST employees all] there was an error with status ${status} error: ${err}`);
    res.status(status).json({ status, error: err.message });
  }
};
