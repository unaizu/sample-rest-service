const Employee = require('../../../models/employee');
const Logger = require('../../../utils/logger');

module.exports = async (req, res) => {
  try {
    // first confirm we've got the Id parameter and object on the request
    // the object would have been added to the request by findObject
    if (req.params.employeeId === undefined || req.employee === undefined) {
      res.status(400).json({ success: false, error: 'bad input parameter' });
      Logger.debug(
        `[REST employees single] bad input, employeeId: ${req.params.employeeId} employee: ${JSON.stringify(
          req.employee
        )}`
      );
      return;
    }
    // if we got here we can just return the object from the directly from the request
    // without the need of searching for it again
    Logger.debug(`[REST employees single] about to return ${JSON.stringify(req.employee)}`);
    res.status(200).json({ status: 200, data: { employees: [req.employee] } });
  } catch (err) {
    const status = err.status ? err.status : 500;
    Logger.error(`[REST employees all] there was an error with status ${status} error: ${err}`);
    res.status(status).json({ status, error: err.message });
  }
};
