const employees = require('express').Router();

// define what the /employees endpoint will allow and its subtree
const all = require('./all');
const single = require('./single');
const findObject = require('../../../utils/findObject');

employees.param('employeeId', findObject('employee'));
employees.get('/:employeeId', single);
employees.get('/', all);

/**
 *
 * if you want to add a new method you can simple define it below
 * for example if you wanted to add a post method that allows user to create new employees
 * you just have to do the following
 *
 * create a file that will handle the post request for example add.js
 * add the following lines:
 * const add = require('./add.js')
 * employees.post('/', add)
 *
 */

module.exports = employees;
