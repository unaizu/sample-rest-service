const routes = require('express').Router();

// define a sample /employees REST endpoint
const employees = require('./employees');

routes.use('/employees', employees);
// if you want to learn how to extend the employees endpoint please refer to the:
// /routes/v1/employees/index.js file

/**
 * creating a new endpoint is simple all you need to do is to create a new subfolder
 * for example weather
 *
 * add a new file on that subfolder called index.js and define the methods
 * that will be allowed simmilarly
 * to what we have done for employees
 *
 * I typically add any update/modify/delete methods inside a subfolder called admin
 * but it's totally up to you how you want to structure your folders
 */

module.exports = routes;
