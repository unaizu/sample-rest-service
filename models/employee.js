const Logger = require('../utils/logger');

class Employee {}

// eslint-disable-next-line no-unused-vars
Employee.getAllEmployees = async (_req, _res) =>
  new Promise((resolve, reject) => {
    try {
      Logger.debug('[getAllEmployees] about to request data for all employees');
      // we would normally query the DB here
      // we will simulate the db query by adding a delay with setTimeout
      setTimeout(() => {
        const employees = [{ Id: 1 }, { Id: 2 }];
        const data = { employees };
        // resolve the sample data
        resolve(data);
      }, 1000);
    } catch (err) {
      Logger.error(`[getAllEmployees] ERROR: ${err}`);
      reject(err);
    }
  });

Employee.getEmployeeWithId = async (id) =>
  new Promise((resolve, reject) => {
    try {
      Logger.debug(`[getEmployeeWithId] about to request data for employee with id: ${id}`);
      // we would normally query the DB here
      // we will simulate the db query by adding a delay with setTimeout
      setTimeout(() => {
        const employees = [{ Id: parseInt(id) }];
        const data = { employees };
        // resolve the sample data
        resolve(data);
      }, 1000);
    } catch (err) {
      Logger.error(`[getEmployeeWithId] ERROR: ${err}`);
      reject(err);
    }
  });

module.exports = Employee;
