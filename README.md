# Sample REST Service

This project is meant to be used as a baseline for any REST applications you want to develop using Nodejs.

## Installation

Git clone or download the project to your machine
run npm install using the following command:

```bash
npm install
```

modify the .env_sample file and save it as .env in order to initialise all required variables

## Extending the project

I've structured the project so all REST endpoints are contained within the /routes/v1/ folder, if you want to add new REST endpoints please refer to the routes/v1/index.js file for more information on how to do this.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
